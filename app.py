from flask import Flask, jsonify, abort, request
import config

app = Flask(__name__)

notes = []


def set_note_with_title(note):
    if note['title'] == '':
        note['title'] = note['content'][:config.N]
    return note


def set_dict_with_title(olddict):
    newdict = []
    for note in olddict:
        newdict.append(set_note_with_title(note))
    return newdict


@app.route('/notes', methods=['POST'])
def create_task():
    if not request.json or 'content' not in request.json:
        abort(400)
    if notes:
        note_id = notes[-1]['id'] + 1
    else:
        note_id = 0
    note = {
        'id': note_id,
        'title': request.json.get('title', ''),
        'content': request.json['content']
    }
    notes.append(note)
    return jsonify(note), 201


@app.route('/notes', methods=['GET'])
def get_notes():
    if request.args:
        [[k, v]] = request.args.items()
        filtered = [t for t in notes if v in t[k]]
        return jsonify(set_dict_with_title(filtered))
    return jsonify(set_dict_with_title(notes))


@app.route('/notes/<int:note_id>', methods=['GET'])
def get_note(note_id):
    note = list(filter(lambda t: t['id'] == note_id, notes))
    if len(note) == 0:
        abort(404)
    return jsonify(set_note_with_title(note[0]))


@app.route('/notes/<int:note_id>', methods=['PUT'])
def update_note(note_id):
    note = list(filter(lambda t: t['id'] == note_id, notes))
    if len(note) == 0:
        abort(404)
    if not request.json:
        abort(400)
    note[0]['title'] = request.json.get('title', note[0]['title'])
    note[0]['content'] = request.json.get('content', note[0]['content'])
    return jsonify(note[0])


@app.route('/notes/<int:note_id>', methods=['DELETE'])
def delete_task(note_id):
    note = list(filter(lambda t: t['id'] == note_id, notes))
    if len(note) == 0:
        abort(404)
    notes.remove(note[0])
    return '', 204


if __name__ == '__main__':
    app.run(debug=True)
